// Incluyo las SDK
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

// Incluyo la configuracion de la base de datos
const firebaseConfig = {
    apiKey: "AIzaSyCubxSOTTlJyhFwMvSEtz52T08PYzMZzaM",
    authDomain: "foro-preguntas.firebaseapp.com",
    databaseURL: "https://foro-preguntas-default-rtdb.firebaseio.com/",
    projectId: "foro-preguntas",
    storageBucket: "foro-preguntas.appspot.com",
    messagingSenderId: "124005559986",
    appId: "1:124005559986:web:5c6315aa0751f0e2dde3ab"
  };

const app = initializeApp(firebaseConfig);
const auth = getAuth();

// Declaro variables asociada a una referencia del HTML
var correoRef = document.getElementById("I_EMAIL");
var passRef = document.getElementById("I_CONTRA");
var ingresarRef = document.getElementById("I_BOTON");

// Eventos llamadores de funciones
ingresarRef.addEventListener("click", logIn);
// Ingresar con el "Enter" dentro del ultimo campo donde nos encontramos
passRef.addEventListener("keypress", (e) => { 
    if (e.key === 'Enter'){
       logIn();
   }})

// Creo la funcion logIn, para que ingrese cuando se clickea en "Iniciar Sesion"
function logIn ()
{
    // Si los campos de correo y contraseña no estan vacios, entra en el condicional
    if((correoRef.value != '') && (passRef.value != '')){

        signInWithEmailAndPassword(auth, correoRef.value, passRef.value) // Paso los parametros de correo y contraseña para iniciar sesion
        .then((userCredential) => {
            const user = userCredential.user;
             // Abro una ventana nueva, para que el usuario lo direccione a la pagina de inicio
            window.location.href = "../index.html";
        })
        .catch((error) => {
            const errorCode = error.code;
            document.getElementById("Indicador_2").value = errorCode; // Mostramos un mensaje si los campos no estan completos

        });
    }
    else
    {
        document.getElementById("Indicador_2").value = "Revisar que los campos de usuario y contraseña esten completos."; // Mostramos un mensaje si los campos no estan completos
    }    
}
