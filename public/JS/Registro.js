// Incluyo las SDK
// Incluyo las SDK de FireBase
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

// Incluyo la configuracion de la base de datos
const firebaseConfig = {
    apiKey: "AIzaSyCubxSOTTlJyhFwMvSEtz52T08PYzMZzaM",
    authDomain: "foro-preguntas.firebaseapp.com",
    databaseURL: "https://foro-preguntas-default-rtdb.firebaseio.com/",
    projectId: "foro-preguntas",
    storageBucket: "foro-preguntas.appspot.com",
    messagingSenderId: "124005559986",
    appId: "1:124005559986:web:5c6315aa0751f0e2dde3ab"
  };

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth();

// Declaro variables asociada a una referencia del HTML
var Correo_REF = document.getElementById("R_EMAIL");
var Contra_1_REF = document.getElementById("R_CONTRA_1");
var Contra_2_REF = document.getElementById("R_CONTRA_2");
var Nombre_REF = document.getElementById("R_NOMBRE");
var Ciudad_REF = document.getElementById("R_CIUDAD");
var Boton_REF = document.getElementById("R_BOTON");

// Eventos llamadores de funciones
Boton_REF.addEventListener("click", RegistroUsuario);
// Ingresar con el "Enter" dentro del ultimo campo donde nos encontramos
Ciudad_REF.addEventListener("keypress", (e) => { 
    if (e.key === 'Enter'){
       RegistroUsuario();
   }})

// Creo la funcion RegistroUsuario, para que ingrese cuando se clickea en "Registrarse"
function RegistroUsuario()
{
    //Chequeamos que ningun campo este vacio...
    if((Correo_REF.value != '') && (Contra_1_REF.value != '') && (Contra_2_REF.value != '') && (Nombre_REF.value != '') && (Ciudad_REF.value != ''))
    {
        // Comprobamos si ambos campos de contraseña son iguales
        if (Contra_2_REF.value != Contra_1_REF.value)
        {
            // Muestro un texto en el campo con ID "Indicador"
            document.getElementById("Indicador").value = "Los campos de contraseña no coinciden";
        }
        else
        {
            createUserWithEmailAndPassword(auth, Correo_REF.value, Contra_1_REF.value) // Enviamos parametros para crear el usuario
            .then((userCredential) => {
                // Guardo la credencial del usuario, para luego almacenarla en la base de datos
                const user = userCredential.user;
                // Ingreso a la funcion DataBaseUser, ingresando como parametro el uid
                DataBaseUser(user.uid)
                // Abro una ventana nueva, para que el usuario inicie sesion
                //window.location.href = "../Paginas/Inicio_Sesion.html";
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                document.getElementById("Indicador").value = errorMessage;
                console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage); // Mostramos por consola el mensaje de error si hubiera
                if(errorCode == 'auth/email-already-in-use')
                {
                    document.getElementById("Indicador").value = "E-mail ya en uso por otro usuario."; // Mostramos un mensaje si el Email ya esta en uso
                }
            });
        }
    }
    else
    {
        document.getElementById("Indicador").value = "Revisar que los campos no esten en blanco"; // Mostramos un mensaje si ocurre algun error en los campos
    }    
}
// Creo una funcion para guardar los datos del usuario
function DataBaseUser(uid) {
    // Declaro algunas variables con datos del usuario
    let Email = Correo_REF.value
    let Nombre = Nombre_REF.value
    let Ciudad = Ciudad_REF.value
    const UserData = {
        uid: uid,
        Name: Nombre,
        Email: Email,
        city: Ciudad,
    };
    // Coloco en blanco los campos que habian sido escritos, una vez que se enviaron los datos a la base de datos
    Correo_REF.value = ""
    Nombre_REF.value = ""
    Ciudad_REF.value = ""
    Contra_2_REF.value = ""
    Contra_1_REF.value = ""
        // Envio a la base de datos, la ciudad, nombre, etc. Vinculado con la UID unica del usuario
    set(ref(database, "users/" + uid), UserData).then(() => {
        window.location.href = "../Paginas/Inicio_Sesion.html";
        document.getElementById("Indicador").value = "Cuenta registrada exitosamente";
    });
}
