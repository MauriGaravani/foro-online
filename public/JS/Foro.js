// Incluyo las SDK de FireBase
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, push, get, remove, onValue, set} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

// Defino la configuracion de la base de datos con sus diversos parametros
const firebaseConfig = {
  apiKey: "AIzaSyCubxSOTTlJyhFwMvSEtz52T08PYzMZzaM",
  authDomain: "foro-preguntas.firebaseapp.com",
  databaseURL: "https://foro-preguntas-default-rtdb.firebaseio.com/",
  projectId: "foro-preguntas",
  storageBucket: "foro-preguntas.appspot.com",
  messagingSenderId: "124005559986",
  appId: "1:124005559986:web:5c6315aa0751f0e2dde3ab",
};

const app = initializeApp(firebaseConfig);
const auth = getAuth();
const database = getDatabase();


// Declaro variables asociada a una referencia del HTML
var D_Registro = document.getElementById("D_REGISTRO");
var B_Sesion = document.getElementById("D_SESION");
var B_Desconexion = document.getElementById("D_DESCONEXION");
var B_Enviar = document.getElementById("F_BOTON");
var Entrada_Foro  = document.getElementById("Foro_Input");
var messagesBody = document.getElementById("messages-body");
var UserID;
var UserUID;
var usuario;

// Referencias con escuchadores de eventos, en este caso, cuando se hace click se ejecuta una funcion determinada
B_Desconexion.addEventListener("click", LogOut);
B_Enviar.addEventListener("click", Nuevo_Posteo);
Foro_Input.addEventListener("keypress", (e) => { 
  if (e.key === 'Enter'){
     Nuevo_Posteo();
 }})

// Cuando se cambie el estado de la sesion, entrara dentro de esta funcion
onAuthStateChanged(auth, (user) => {

  // Hago invisible el boton de desconexion, y visible el de registro e inicio de sesion 
  B_Sesion.style.display = "block";
  D_Registro.style.display = "block";
  B_Enviar.style.display = "none";
  B_Desconexion.style.display = "none";
  document.getElementById("Foro_Input").placeholder="Inicie Sesion/Registrese para realizar un comentario...";

  // Si el usuario esta conectado, entrara dentro del condicional
  if (user) {
      // Hago visible el boton de desconexion, e invisible el de registro e inicio de sesion 
      B_Sesion.style.display = "none";
      D_Registro.style.display = "none";
      B_Desconexion.style.display = "block";
      B_Enviar.style.display = "block";
      
      // Traigo de la base de datos, mediante el metodo get, los datos del usuario iniciado
      get(ref(database, "users/" + user.uid)).then((snap) => {
          usuario = snap.val();
          UserID = usuario.Name;
          UserUID = user.uid;
          document.getElementById("Foro_Input").placeholder="Hola " + usuario.Name +"! Queremos saber que opina la gente de "+usuario.city+"...";
              // Muestro el nombre del usuario en el navvar
          document.getElementById("navbarDropdownMenuLink").innerHTML = usuario.Name;
      })
  }
});

// Funcion llamada LogOut, creada para desconectar al usuario
function LogOut() {
  auth.signOut();
  location.reload(true);
}
// Funcion llamada Nuevo_Posteo, creada para que el usuario pueda crear un posteo nuevo
function Nuevo_Posteo() {
    let message = Entrada_Foro.value;
    if (!message) return; //Si no hay ningun mensaje no realizo nada
    // Envio mediante el metodo push, el mensaje a la base de datos, asociada con un nombre y ciudad del escritor
    push(ref(database, "Mensajes/"), {
        autor: UserID,
        mensaje: message,
        ciudad: usuario.city,
        uid: UserUID,
    });
    Entrada_Foro.value = '';
} 
// Mediante el metodo onValue, recibo los cambios en la base de datos, luego los muestro en el foro
onValue(ref(database, 'Mensajes/'), (snapshot) => {
  const messages = snapshot.val();
  messagesBody.innerHTML = '';
  // Leo todos los mensajes de la base de datos, y los muestro en el foro
  for(const i in messages) {
    let message = messages[i];
    const campo_txt = document.createElement("div")
    campo_txt.classList.add('Mensaje')
    const p = document.createElement("p")
    p.classList.add('message-forum')
    p.innerText = message.autor + " (" + message.ciudad + ") " + ": " + message.mensaje;
    const Boton_Edit = document.createElement("button")
    Boton_Edit.classList.add('btn' , 'Boton_Editar')
    Boton_Edit.setAttribute("id", [i]);
    Boton_Edit.innerText = "Editar";
    const Boton_Clear = document.createElement("button")
    Boton_Clear.classList.add('btn' , 'Boton_Eliminar')
    Boton_Clear.setAttribute("id", [i]);
    Boton_Clear.innerText = "Eliminar";

    messagesBody.appendChild(campo_txt).appendChild(p)
    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
    if (user) {
    const uid = user.uid;
    if(uid == message.uid){
      messagesBody.appendChild(campo_txt).appendChild(Boton_Edit)
      messagesBody.appendChild(campo_txt).appendChild(Boton_Clear)
      }
      else{
      messagesBody.appendChild(campo_txt)
      messagesBody.appendChild(campo_txt)
      }
    }
    });
   
    Boton_Clear.addEventListener("click", (e) => {
      Eliminar([i]);
    })
    Boton_Edit.addEventListener("click", (e) => {
      Editar([i]);
    })
  };
});

// Funcion para editar comentarios
function Editar(Identificacion)
{
  let message = Entrada_Foro.value;
  set(ref(database, "Mensajes/" + Identificacion), {
    autor: UserID,
    mensaje: message,
    ciudad: usuario.city,
    uid: UserUID,
});
Entrada_Foro.value = "";
document.getElementById("Foro_Input").placeholder="Para editar, primero escribe el comentario aqui, y luego presiona editar en el comentario que desees!";
}

// Funcion para eliminar comentarios
function Eliminar(Identificacion)
{
  console.log("BORRANDO post: " + Identificacion);
  remove(ref(database, "Mensajes/" + Identificacion));
}